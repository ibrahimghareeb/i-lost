package com.example.lostfound;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lostfound.model.ResponseLoginModel;
import com.google.android.gms.maps.model.LatLng;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Signup extends AppCompatActivity {
    EditText fullName;
    EditText email;
    EditText password;
    EditText mobile;
    TextView location;
    Button signup;
    LatLng latLng;
    Retrofit retrofit;
    RetrofitInterface retrofitInterface;
    String BASE_URL="https://cars-leas.000webhostapp.com/lostthings/";
    SharedPreferences.Editor editor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        fullName=(EditText) findViewById(R.id.fullName);
        email=(EditText) findViewById(R.id.email);
        password=(EditText) findViewById(R.id.editPassword);
        location=(TextView) findViewById(R.id.userLocation);
        signup=(Button) findViewById(R.id.signup);
        mobile=(EditText) findViewById(R.id.mobile);
        retrofit =new Retrofit.Builder().
                baseUrl(BASE_URL).
                addConverterFactory(GsonConverterFactory.create())
                .build();
        retrofitInterface = retrofit.create(RetrofitInterface.class);
        editor=getSharedPreferences("myuser", Context.MODE_PRIVATE).edit();
    }
    public boolean checkInputField(){
        if (fullName.getText().toString().equals("")){
            Toast.makeText(Signup.this,"Please enter your name",Toast.LENGTH_SHORT).show();
            return false;
        }
        if (email.getText().toString().equals("") || !email.getText().toString().contains("@")){
            Toast.makeText(Signup.this,"Please enter valid email ",Toast.LENGTH_SHORT).show();
            return false;
        }
        if (password.getText().toString().equals("") || password.getText().toString().length()<6){
            Toast.makeText(Signup.this,"Please Use 6 characters or more for your password ",Toast.LENGTH_SHORT).show();
            return false;
        }
        if(location.getText().toString().contains("Enter")){
            Toast.makeText(Signup.this,"Please Enter your location",Toast.LENGTH_SHORT).show();
            return false;
        }
        if(mobile.getText().toString().equals("")){
            Toast.makeText(Signup.this,"Please Enter your phone number",Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
    public void chooseLocation(View v){
        Intent in=new Intent(Signup.this,MapsActivity.class);
        startActivityForResult(in,1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==1&&resultCode==RESULT_OK){
            Bundle bundle = data.getParcelableExtra("latlang");
            latLng=bundle.getParcelable("latlang");
            location.setText(String.valueOf(latLng.latitude)+" "+String.valueOf(latLng.longitude));
        }
    }
    public void signupNow(View v){
        if(!checkInputField()){
            return;
        }
        Call<ResponseLoginModel> call=retrofitInterface.executeSignup(email.getText().toString(),
                password.getText().toString(),
                fullName.getText().toString(),
                String.valueOf(latLng.latitude),
                String.valueOf(latLng.longitude),
                mobile.getText().toString());
        call.enqueue(new Callback<ResponseLoginModel>() {
            @Override
            public void onResponse(Call<ResponseLoginModel> call, Response<ResponseLoginModel> response) {
                if(response.code()==200){
                    ResponseLoginModel responseLoginModel=response.body();
                    if (responseLoginModel.getError()==0){
                        editor.putBoolean("Login",true).commit();
                        editor.putString("email",email.getText().toString()).commit();
                        editor.putString("password",password.getText().toString()).commit();
                        editor.putInt("userId",responseLoginModel.getId()).commit();
                        editor.putString("fullname",responseLoginModel.getName()).commit();
                        Intent in=new Intent(Signup.this,MainActivity.class);
                        startActivity(in);
                        finish();
                    }else{
                        Toast.makeText(Signup.this,responseLoginModel.getAlert(),Toast.LENGTH_LONG).show();
                    }

                }
            }
            @Override
            public void onFailure(Call<ResponseLoginModel> call, Throwable t) {
                Toast.makeText(Signup.this,t.toString(),Toast.LENGTH_LONG).show();
            }
        });
    }
}
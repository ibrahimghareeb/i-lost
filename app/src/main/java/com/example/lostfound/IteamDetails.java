package com.example.lostfound;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lostfound.adapter.ImageAdapter;
import com.example.lostfound.model.ItemModel;
import com.example.lostfound.model.ItemModelForView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
;import java.text.DecimalFormat;
import java.text.ParseException;

public class IteamDetails extends AppCompatActivity implements OnMapReadyCallback {
    MapView map;
    TextView itemName;
    TextView founderName;
    TextView description;
    TextView mail;
    TextView mobile;
    ImageButton whatsappButton;
    double lng;
    double lat;
    private ItemModelForView share;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_iteam_details);
        Intent in=getIntent();
        if(in.getIntExtra("item",0)==1)
            share= LostItemFragment.share;
        else if(in.getIntExtra("item",0)==2)
            share=FoundItemFragment.foundShare;
        else
            share=SearchFragment.searchShare;
        Bitmap img= share.imageUrl;
        ViewPager mViewPager = (ViewPager) findViewById(R.id.viewpager);
        ImageAdapter adapterView = new ImageAdapter(this,img);
        mViewPager.setAdapter(adapterView);
        itemName=(TextView)findViewById(R.id.nameItemDetail);
        founderName=(TextView)findViewById(R.id.itemfounder);
        description=(TextView)findViewById(R.id.descriptionItem);
        mail =(TextView) findViewById(R.id.mailInfoItemDetail);
        mobile=(TextView) findViewById(R.id.mobileInfoItemDetail);
        String temp=share.itemModel.getLatitude().replaceAll("\"","");
        lat=new Double(temp);
        temp=share.itemModel.getLongitude().replaceAll("\"","");
        lng=new Double(temp);
        Toast.makeText(this,share.itemModel.getLatitude()+" "+share.itemModel.getLongitude(), Toast.LENGTH_SHORT).show();
        Toast.makeText(this, String.valueOf(lng)+" "+String.valueOf(lat), Toast.LENGTH_SHORT).show();
        itemName.setText(share.itemModel.getNameItem());
        founderName.setText(share.itemModel.getUser_name());
        description.setText(share.itemModel.getDescription());
        mail.setText(share.itemModel.getEmail());
        mobile.setText(share.itemModel.getMobile());
        map=(MapView)findViewById(R.id.map2) ;
        map.getMapAsync(this);
        map.onCreate(savedInstanceState);
        whatsappButton=(ImageButton)findViewById(R.id.whatsappButton);
        whatsappButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String message;
                if(in.getIntExtra("item",0)==1)
                    message="hi you found my "+share.itemModel.getNameItem();
                else
                    message="hi i found yor "+share.itemModel.getNameItem();
                Uri uri=Uri.parse("https://api.whatsapp.com/send?phone="+share.itemModel.getMobile()+"&text="+message);
                Intent i=new Intent(Intent.ACTION_VIEW,uri);
                i.setPackage("com.whatsapp");
                startActivity(i);
            }
        });

    }

    @Override
    public void onMapReady(@NonNull GoogleMap googleMap) {
        LatLng myLocation = new LatLng(lat, lng);
        googleMap.addMarker(new MarkerOptions().position(myLocation).title("i'm here"));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(myLocation));
    }
    @Override
    public void onResume() {
        super.onResume();
        map.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
        map.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        map.onStop();
    }


    @Override
    public void onPause() {
        map.onPause();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        map.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        map.onLowMemory();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState, @NonNull PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
    }
}
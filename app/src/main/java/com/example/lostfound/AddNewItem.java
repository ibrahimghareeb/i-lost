package com.example.lostfound;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lostfound.model.CategoryModel;
import com.example.lostfound.model.CategoryModelList;
import com.example.lostfound.model.ResponseLoginModel;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;


public class AddNewItem extends Fragment {
    int typeOfItem;
    EditText itemName;
    EditText city;
    Spinner spinner;
    EditText description;
    Button pickImage;
    Button send;
    LatLng latLng;
    Spinner category;
    TextView location;
    Retrofit retrofit;
    ArrayList<String> arrayList;
    ArrayAdapter<String> arrayAdapter;
    CategoryModelList categoryModelList;
    RetrofitInterface retrofitInterface;
    String BASE_URL = "https://cars-leas.000webhostapp.com/lostthings/";
    Bitmap img;
    SharedPreferences sharedPreferences;
    private final int IMAGE_REQUEST_ID = 1;
    private int RESULT_OK = -1;
    private final int LOCATION_REQUEST_ID = 2;
    Uri temp;

    public AddNewItem(int typeOfItem) {
        this.typeOfItem = typeOfItem;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root=inflater.inflate(R.layout.fragment_add_new_item, container, false);
        if(typeOfItem==1){
            MainActivity.actionBar.setTitle("Add new lost Item");
        }else{
            MainActivity.actionBar.setTitle("Add new found Item");
        }
        sharedPreferences=getActivity().getSharedPreferences("myuser", Context.MODE_PRIVATE);
        pickImage=(Button)root.findViewById(R.id.PickImage) ;
        send=(Button)root.findViewById(R.id.Send);
        itemName=(EditText)root.findViewById(R.id.itemName);
        city=(EditText)root.findViewById(R.id.city);
        description=(EditText)root.findViewById(R.id.itemDescription);
        location=(TextView)root.findViewById(R.id.itemLocation) ;
        category=(Spinner)root.findViewById(R.id.category) ;
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        retrofit =new Retrofit.Builder().
                baseUrl(BASE_URL).
                addConverterFactory(GsonConverterFactory.create(gson))
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();
        retrofitInterface = retrofit.create(RetrofitInterface.class);
        initializeSpinner(root);
        location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in=new Intent(getActivity(),MapsActivity.class);
                startActivityForResult(in,LOCATION_REQUEST_ID);
            }
        });
        pickImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(Intent.createChooser(in, "choose Image"), IMAGE_REQUEST_ID);
            }
        });

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                File file=new File(getRealPathFromURI(temp));
                if (checkInputField()) {
                    RequestBody image = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                    int catId=getCategoryId(spinner.getSelectedItem().toString());
                    Call<ResponseLoginModel> call = retrofitInterface.executeAddItem(sharedPreferences.getInt("userId", -18),
                            itemName.getText().toString(),
                            description.getText().toString(),
                            city.getText().toString(),
                            String.valueOf(latLng.latitude),
                            String.valueOf(latLng.longitude),
                            MultipartBody.Part.createFormData("Image[]",file.getName(),image),
                            typeOfItem,
                            catId);
                    call.enqueue(new Callback<ResponseLoginModel>() {
                        @Override
                        public void onResponse(Call<ResponseLoginModel> call, Response<ResponseLoginModel> response) {
                            ResponseLoginModel responseLoginModel = response.body();
                            if(responseLoginModel.getError()==0){
                                Toast.makeText(getActivity(), "Item added successfully", Toast.LENGTH_SHORT).show();
                                if (typeOfItem==1){
                                    LostItemFragment lostItemFragment=new LostItemFragment();
                                    getFragmentManager().beginTransaction().replace(R.id.nav_host_fragment_content_main,lostItemFragment).commit();
                                }else{
                                    FoundItemFragment lostItemFragment=new FoundItemFragment();
                                    getFragmentManager().beginTransaction().replace(R.id.nav_host_fragment_content_main,lostItemFragment).commit();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseLoginModel> call, Throwable t) {
                            Toast.makeText(getActivity(), t.toString()+" fail", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });
        return root;
    }

    private int getCategoryId(String selectedItem) {
        for (CategoryModel i:categoryModelList.getData()){
            if(i.getName_cat().equals(selectedItem))
                return i.getId_cat();
        }
        return 0;
    }

    private void initializeSpinner(View view) {
        Call<CategoryModelList> call=retrofitInterface.executeGetCategory(sharedPreferences.getInt("userId",-16));
        call.enqueue(new Callback<CategoryModelList>() {
            @Override
            public void onResponse(Call<CategoryModelList> call, Response<CategoryModelList> response) {
                if (response.code() == 200) {
                    categoryModelList=response.body();
                    arrayList=new ArrayList<>();
                    for (CategoryModel i : categoryModelList.getData()){
                        arrayList.add(i.getName_cat());
                    }
                    spinner=(Spinner) view.findViewById(R.id.category);
                    arrayAdapter=new ArrayAdapter<String>(getActivity(),android.R.layout.simple_spinner_item, arrayList);
                    arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinner.setAdapter(arrayAdapter);
                }
            }
            @Override
            public void onFailure(Call<CategoryModelList> call, Throwable t) {
                Toast.makeText(getActivity(), t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public boolean checkInputField (){
        if(itemName.getText().toString().equals("")){
            Toast.makeText(getActivity(),"Enter item name",Toast.LENGTH_LONG).show();
            return false;
        }
        if(city.getText().toString().equals("")){
            Toast.makeText(getActivity(),"Enter item city",Toast.LENGTH_LONG).show();
            return false;
        }
        if(description.getText().toString().equals("")){
            Toast.makeText(getActivity(),"Enter item name",Toast.LENGTH_LONG).show();
            return false;
        }
        if(img==null){
            Toast.makeText(getActivity(),"Enter photo",Toast.LENGTH_LONG).show();
            return false;
        }
        if(latLng==null){
            Toast.makeText(getActivity(),"Enter Location",Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }


    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == IMAGE_REQUEST_ID && resultCode == RESULT_OK
                && data != null) {
            try {
                img = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
                temp=data.getData();
            } catch (IOException e) {
                Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_SHORT).show();
            }
        }
        if(requestCode==LOCATION_REQUEST_ID&&resultCode==RESULT_OK){
            Bundle bundle = data.getParcelableExtra("latlang");
            latLng=bundle.getParcelable("latlang");
            location.setText(String.valueOf(latLng.latitude)+" "+String.valueOf(latLng.longitude));
        }

    }


    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }
}
package com.example.lostfound;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.lostfound.adapter.ItemAdapter;
import com.example.lostfound.model.CategoryModel;
import com.example.lostfound.model.CategoryModelList;
import com.example.lostfound.model.ItemModel;
import com.example.lostfound.model.ItemModelForView;
import com.example.lostfound.model.ItemModelList;
import com.example.lostfound.model.ItemPicture;
import com.example.lostfound.model.ResponseLoginModel;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class SearchFragment extends Fragment implements ItemAdapter.ItemListener{
    Spinner spinner;
    Spinner category;
    String spinner_option[]={"Lost Item","Found Item"};
    ArrayAdapter <String> arrayAdapter;
    SharedPreferences sharedPreferences;
    EditText itemName;
    EditText city;
    Button search;
    Retrofit retrofit;
    RecyclerView recyclerView;
    ItemModelList itemModelList;
    Bitmap imag;
    List<ItemModelForView> itemModelForViewList;
    public static ItemModelForView searchShare;
    RetrofitInterface retrofitInterface;
    String BASE_URL="https://cars-leas.000webhostapp.com/lostthings/";
    private CategoryModelList categoryModelList;
    private ArrayList<String> arrayListForCategory;
    private ArrayAdapter<String> arrayAdapterForCategory;
    private ItemAdapter itemAdapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_search, container, false);
        recyclerView=(RecyclerView)view.findViewById(R.id.recyclerViewForSearche);
        sharedPreferences=getActivity().getSharedPreferences("myuser", Context.MODE_PRIVATE);
        itemName=(EditText) view.findViewById(R.id.search_item_name);
        city=(EditText) view.findViewById(R.id.search_item_city);
        search=(Button) view.findViewById(R.id.search_button);
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        retrofit =new Retrofit.Builder().
                baseUrl(BASE_URL).
                addConverterFactory(GsonConverterFactory.create(gson))
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();
        retrofitInterface = retrofit.create(RetrofitInterface.class);
        initializeSpinner(view);
        itemModelForViewList =new ArrayList<>();
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int cateId=getCategoryId(category.getSelectedItem().toString());
                Call<ItemModelList> call=retrofitInterface.executeSearch(sharedPreferences.getInt("userId",-12),itemName.getText().toString(),city.getText().toString(),String.valueOf(spinner.getSelectedItemId()+1),String.valueOf(cateId));
                call.enqueue(new Callback<ItemModelList>() {
                    @Override
                    public void onResponse(Call<ItemModelList> call, Response<ItemModelList> response) {
                        if(response.code()==200 && response.body().getError()==0){
                            itemModelList=response.body();
                            for(ItemModel i:itemModelList.getData()){
                                ItemPicture image;
                                if (!i.getPictures().isEmpty()){
                                    for (ItemPicture j:i.getPictures()){
                                        image=j;
                                        Call<ResponseBody> call2=retrofitInterface.executeGetImage(image.getUrl());
                                        call2.enqueue(new Callback<ResponseBody>() {
                                            @Override
                                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                                if(response.isSuccessful()){
                                                    InputStream is=response.body().byteStream();
                                                    imag = BitmapFactory.decodeStream(is);
                                                    itemModelForViewList.add(new ItemModelForView(i,imag));
                                                    printRecyclerView(itemModelForViewList);
                                                }
                                            }
                                            @Override
                                            public void onFailure(Call<ResponseBody> call, Throwable t) {
                                                Toast.makeText(getActivity(), t.toString(), Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                    }
                                }
                                //itemModelForViewList.add(new ItemModelForView(i.getNameItem(),i.getCity(),i.getMobile(),i.getMobile(), Bitmap.))
                            }
                        }else
                            Toast.makeText(getActivity(), response.body().getAlert()+" "+String.valueOf(spinner.getSelectedItemId()+1)+" "+String.valueOf(cateId), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailure(Call<ItemModelList> call, Throwable t) {
                        Toast.makeText(getActivity(), t.toString(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
        return view;
    }



    void initializeSpinner(View view){
        spinner=(Spinner) view.findViewById(R.id.spinner);
        category=(Spinner)view.findViewById(R.id.spinnerCategorySearch);
        arrayAdapter=new ArrayAdapter<String>(getActivity(),android.R.layout.simple_spinner_item, spinner_option);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(arrayAdapter);
        Call<CategoryModelList> call=retrofitInterface.executeGetCategory(sharedPreferences.getInt("userId",-16));
        call.enqueue(new Callback<CategoryModelList>() {
            @Override
            public void onResponse(Call<CategoryModelList> call, Response<CategoryModelList> response) {
                if (response.code() == 200) {
                    categoryModelList=response.body();
                    arrayListForCategory=new ArrayList<>();
                    for (CategoryModel i : categoryModelList.getData()){
                        arrayListForCategory.add(i.getName_cat());
                    }
                    arrayAdapterForCategory=new ArrayAdapter<String>(getActivity(),android.R.layout.simple_spinner_item, arrayListForCategory);
                    arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    category.setAdapter(arrayAdapterForCategory);
                }
            }
            @Override
            public void onFailure(Call<CategoryModelList> call, Throwable t) {
                Toast.makeText(getActivity(), t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private int getCategoryId(String selectedItem) {
        for (CategoryModel i:categoryModelList.getData()){
            if(i.getName_cat().equals(selectedItem))
                return i.getId_cat();
        }
        return 0;
    }
    private void printRecyclerView(List<ItemModelForView> itemModelForViewList) {
        itemAdapter=new ItemAdapter(itemModelForViewList,this);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(itemAdapter);
    }

    @Override
    public void onClick(int position) {
        Intent in=new Intent(getActivity(),IteamDetails.class);
        searchShare=itemModelForViewList.get(position);
        in.putExtra("item",3);
        startActivity(in);
    }
}
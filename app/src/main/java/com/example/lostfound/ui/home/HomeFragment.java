package com.example.lostfound.ui.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.lostfound.FoundItemFragment;
import com.example.lostfound.LostItemFragment;
import com.example.lostfound.MainActivity;
import com.example.lostfound.R;
import com.example.lostfound.SearchFragment;
import com.example.lostfound.databinding.FragmentHomeBinding;

public class HomeFragment extends Fragment {

    private FragmentHomeBinding binding;
    Button lostItem;
    Button foundItem;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        binding = FragmentHomeBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        lostItem=binding.lostItem;
        lostItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LostItemFragment lostItemFragment =new LostItemFragment();
                getFragmentManager().beginTransaction().replace(R.id.nav_host_fragment_content_main,lostItemFragment).commit();
            }
        });
        setHasOptionsMenu(true);
        foundItem=binding.foundItem;
        foundItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FoundItemFragment foundItemFragment=new FoundItemFragment();
                getFragmentManager().beginTransaction().replace(R.id.nav_host_fragment_content_main,foundItemFragment).commit();
            }
        });
        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_search:
                MainActivity.actionBar.setTitle("search");
                SearchFragment searchFragment=new SearchFragment();
                getFragmentManager().beginTransaction().replace(R.id.nav_host_fragment_content_main,searchFragment).commit();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
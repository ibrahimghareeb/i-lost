package com.example.lostfound.ui.edit_profile;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.lostfound.MainActivity;
import com.example.lostfound.MapsActivity;
import com.example.lostfound.R;
import com.example.lostfound.RetrofitInterface;
import com.example.lostfound.SearchFragment;
import com.example.lostfound.Signup;
import com.example.lostfound.databinding.FragmentEditProfileBinding;
import com.example.lostfound.model.ResponseLoginModel;
import com.google.android.gms.maps.model.LatLng;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class EditProfile extends Fragment {
    private FragmentEditProfileBinding binding;
    SharedPreferences sharedPreferences;
    EditText fullName;
    EditText email;
    EditText password;
    TextView location;
    EditText mobile;
    Button edit;
    LatLng latLng;
    Retrofit retrofit;
    RetrofitInterface retrofitInterface;
    String BASE_URL="https://cars-leas.000webhostapp.com/lostthings/";
    SharedPreferences.Editor editor;
    private int RESULT_OK=-1;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentEditProfileBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        setHasOptionsMenu(true);
        fullName=(EditText) binding.editFullName;
        email=(EditText) binding.editEmail;
        password=(EditText) binding.editPassword;
        location=(TextView) binding.editLocation;
        edit=(Button) binding.edit;
        mobile=binding.editMobile;
        retrofit =new Retrofit.Builder().
                baseUrl(BASE_URL).
                addConverterFactory(GsonConverterFactory.create())
                .build();
        retrofitInterface = retrofit.create(RetrofitInterface.class);
        sharedPreferences=getActivity().getSharedPreferences("myuser", Context.MODE_PRIVATE);
        editor=getActivity().getSharedPreferences("myuser", Context.MODE_PRIVATE).edit();
        ResponseLoginModel responseLoginModel;
        location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in=new Intent(getActivity(), MapsActivity.class);
                startActivityForResult(in,2);
            }
        });

        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkInputField()) {
                    Call<ResponseLoginModel> call = retrofitInterface.executeEditProfile(
                            sharedPreferences.getInt("userId", -18),
                            email.getText().toString(),
                            password.getText().toString(),
                            fullName.getText().toString(),
                            String.valueOf(latLng.latitude),
                            String.valueOf(latLng.longitude),
                            mobile.getText().toString());
                    call.enqueue(new Callback<ResponseLoginModel>() {
                        @Override
                        public void onResponse(Call<ResponseLoginModel> call, Response<ResponseLoginModel> response) {
                            if (response.code() == 200) {
                                ResponseLoginModel responseLoginModel = response.body();
                                if (responseLoginModel.getError() == 0) {
                                    editor.putString("email", email.getText().toString()).commit();
                                    editor.putString("password", password.getText().toString()).commit();
                                    editor.putInt("userId", responseLoginModel.getId()).commit();
                                    Toast.makeText(getActivity(), responseLoginModel.getAlert(), Toast.LENGTH_LONG).show();
                                } else {
                                    Toast.makeText(getActivity(), responseLoginModel.getAlert(), Toast.LENGTH_LONG).show();
                                }

                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseLoginModel> call, Throwable t) {
                            Toast.makeText(getActivity(), t.toString(), Toast.LENGTH_LONG).show();
                        }
                    });
                }
                }
            });

        return root;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(requestCode==2&&resultCode==RESULT_OK){
            Bundle bundle = data.getParcelableExtra("latlang");
            latLng=bundle.getParcelable("latlang");
            location.setText(String.valueOf(latLng.latitude)+" "+String.valueOf(latLng.longitude));
        }
    }
    public boolean checkInputField(){
        if (fullName.getText().toString().equals("")){
            Toast.makeText(getActivity(),"Please enter your name",Toast.LENGTH_SHORT).show();
            return false;
        }
        if (email.getText().toString().equals("") || !email.getText().toString().contains("@")){
            Toast.makeText(getActivity(),"Please enter valid email ",Toast.LENGTH_SHORT).show();
            return false;
        }
        if (password.getText().toString().equals("") || password.getText().toString().length()<6){
            Toast.makeText(getActivity(),"Please Use 6 characters or more for your password ",Toast.LENGTH_SHORT).show();
            return false;
        }
        if(location.getText().toString().contains("Enter")){
            Toast.makeText(getActivity(),"Please Enter your location",Toast.LENGTH_SHORT).show();
            return false;
        }
        if(mobile.getText().toString().equals("")){
            Toast.makeText(getActivity(),"Please Enter your phone number",Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_search:
                SearchFragment searchFragment=new SearchFragment();

                getFragmentManager().beginTransaction().replace(R.id.nav_host_fragment_content_main,searchFragment).commit();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
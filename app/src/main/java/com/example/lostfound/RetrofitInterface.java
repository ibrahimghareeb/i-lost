package com.example.lostfound;

import android.graphics.Bitmap;

import com.example.lostfound.model.CategoryModel;
import com.example.lostfound.model.CategoryModelList;
import com.example.lostfound.model.ItemModelList;
import com.example.lostfound.model.ResponseLoginModel;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Url;

public interface RetrofitInterface {
    @FormUrlEncoded
    @POST("login.php")
    Call<ResponseLoginModel> executeLogin(@Field("email") String email, @Field("password") String password);

    @FormUrlEncoded
    @POST("register.php")
    Call<ResponseLoginModel> executeSignup(@Field("email") String email, @Field("password") String password, @Field("name") String name, @Field("latitude") String latitude, @Field("longitude") String longitude,@Field("mobile") String mobile);

    @FormUrlEncoded
    @POST("editProfile.php")
    Call<ResponseLoginModel> executeEditProfile(@Field("userId") int id,@Field("email") String email, @Field("password") String password, @Field("name") String name,@Field("mobile") String mobile, @Field("latitude") String latitude, @Field("longitude") String longitude);

    @FormUrlEncoded
    @POST("search.php")
    Call<ItemModelList> executeSearch(@Field("userId") int id,@Field("nameItem") String nameItem,@Field("city") String city,@Field("type") String type,@Field("id_cat") String id_cat);

    @Multipart
    @POST("AddItem.php")
    Call<ResponseLoginModel> executeAddItem(@Part("userId") int id, @Part("nameItem") String email, @Part("description") String description, @Part("city") String city, @Part("latitude") String latitude, @Part("longitude") String longitude, @Part MultipartBody.Part image, @Part("type")int type, @Part("id_cat") int category);

    @FormUrlEncoded
    @POST("AllCategory.php")
    Call <CategoryModelList> executeGetCategory(@Field("userId") int userId);
    @FormUrlEncoded
    @POST("AllItems.php")
    Call<ItemModelList> executeAllItems(@Field("userId")int id,@Field("type") int type);
    @GET
    Call<ResponseBody> executeGetImage(@Url String url);
}
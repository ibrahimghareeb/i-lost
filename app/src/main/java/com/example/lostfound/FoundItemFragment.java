package com.example.lostfound;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.lostfound.adapter.ItemAdapter;
import com.example.lostfound.model.ItemModel;
import com.example.lostfound.model.ItemModelForView;
import com.example.lostfound.model.ItemModelList;
import com.example.lostfound.model.ItemPicture;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class FoundItemFragment extends Fragment implements ItemAdapter.ItemListener {
    RecyclerView recyclerViewForFoundItem;
    List<ItemModelForView> itemModelForViewList;
    ItemModelList itemModelList;
    SharedPreferences sharedPreferences;
    Retrofit retrofit;
    Bitmap imag;
    RetrofitInterface retrofitInterface;
    String BASE_URL="https://cars-leas.000webhostapp.com/lostthings/";
    Button addNewFoundItem;
    private ItemAdapter itemAdapter;
    public static ItemModelForView foundShare;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_found_item, container, false);
        MainActivity.actionBar.setTitle("Found Item");
        recyclerViewForFoundItem = (RecyclerView) root.findViewById(R.id.foundItemRecyclerView);
        addNewFoundItem=root.findViewById(R.id.addNewFoundItem);
        sharedPreferences=getActivity().getSharedPreferences("myuser", Context.MODE_PRIVATE);
        addNewFoundItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddNewItem addNewItem=new AddNewItem(2);
                getFragmentManager().beginTransaction().replace(R.id.nav_host_fragment_content_main,addNewItem).commit();
            }
        });

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        retrofit =new Retrofit.Builder().
                baseUrl(BASE_URL).
                addConverterFactory(GsonConverterFactory.create(gson))
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();
        retrofitInterface = retrofit.create(RetrofitInterface.class);
        itemModelForViewList =new ArrayList<>();
        Call<ItemModelList> call=retrofitInterface.executeAllItems(sharedPreferences.getInt("userId",-18),1);
        call.enqueue(new Callback<ItemModelList>() {
            @Override
            public void onResponse(Call<ItemModelList> call, Response<ItemModelList> response) {
                if(response.code()==200 && response.body().getError()==0){
                    itemModelList=response.body();
                    for(ItemModel i:itemModelList.getData()){
                        ItemPicture image;
                        if (!i.getPictures().isEmpty()){
                            for (ItemPicture j:i.getPictures()){
                                image=j;
                                Call<ResponseBody> call2=retrofitInterface.executeGetImage(image.getUrl());
                                call2.enqueue(new Callback<ResponseBody>() {
                                    @Override
                                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                        if(response.isSuccessful()){
                                            InputStream is=response.body().byteStream();
                                            imag = BitmapFactory.decodeStream(is);
                                            itemModelForViewList.add(new ItemModelForView(i,imag));
                                            printRecyclerView(itemModelForViewList);
                                        }
                                    }
                                    @Override
                                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                                        Toast.makeText(getActivity(), t.toString(), Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }
                        }
                        //itemModelForViewList.add(new ItemModelForView(i.getNameItem(),i.getCity(),i.getMobile(),i.getMobile(), Bitmap.))
                    }
                }

            }

            @Override
            public void onFailure(Call<ItemModelList> call, Throwable t) {
                Toast.makeText(getActivity(), t.toString(), Toast.LENGTH_SHORT).show();
            }
        });

        return root;
    }

    private void printRecyclerView(List<ItemModelForView> itemModelForViewList) {
        itemAdapter=new ItemAdapter(itemModelForViewList,this);
        recyclerViewForFoundItem.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerViewForFoundItem.setItemAnimator(new DefaultItemAnimator());
        recyclerViewForFoundItem.setAdapter(itemAdapter);
    }

    @Override
    public void onClick(int position) {
        Intent in=new Intent(getActivity(),IteamDetails.class);
        foundShare=itemModelForViewList.get(position);
        in.putExtra("item",2);
        startActivity(in);
    }


}
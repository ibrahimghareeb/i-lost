package com.example.lostfound.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.lostfound.R;
import com.example.lostfound.model.ItemModelForView;

import java.util.List;

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ViewHolder>{
    List <ItemModelForView> itemModelForViews;
    ItemListener itemListener;
    public ItemAdapter(List<ItemModelForView> itemModelForViews, ItemListener itemListener) {
        this.itemModelForViews = itemModelForViews;
        this.itemListener = itemListener;
    }

    @NonNull
    @Override
    public ItemAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.found_item_view,parent,false);
        ViewHolder vh= new ViewHolder(v, itemListener);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.itemName.setText(itemModelForViews.get(position).itemModel.getNameItem());
        holder.itemLocation.setText(itemModelForViews.get(position).itemModel.getCity());
        holder.contact.setText(itemModelForViews.get(position).itemModel.getEmail());
        holder.imageView.setImageBitmap(itemModelForViews.get(position).imageUrl);
    }
    @Override
    public int getItemCount() {
        return itemModelForViews.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView itemName;
        TextView itemLocation;
        ImageView imageView;
        TextView contact;
        ItemListener itemListener;
        public ViewHolder(@NonNull View itemView, ItemListener itemListener) {
            super(itemView);
            itemName=(TextView) itemView.findViewById(R.id.item_name);
            itemLocation=(TextView) itemView.findViewById(R.id.itemCity);
            contact=(TextView) itemView.findViewById(R.id.foundContactInfo);
            imageView=(ImageView) itemView.findViewById(R.id.foundItemImage);
            this.itemListener = itemListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            itemListener.onClick(getAdapterPosition());
        }

    }
    public interface ItemListener {
        void onClick(int position);
    }
}

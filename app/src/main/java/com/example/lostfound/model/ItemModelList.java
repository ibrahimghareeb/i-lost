package com.example.lostfound.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ItemModelList {
    @SerializedName("data")
    List<ItemModel> data;
    int Error;
    String alert;

    public ItemModelList(List<ItemModel> data, int error, String alert) {
        this.data = data;
        Error = error;
        this.alert = alert;
    }

    public List<ItemModel> getData() {
        return data;
    }

    public void setData(List<ItemModel> data) {
        this.data = data;
    }

    public int getError() {
        return Error;
    }

    public void setError(int error) {
        Error = error;
    }

    public String getAlert() {
        return alert;
    }

    public void setAlert(String alert) {
        this.alert = alert;
    }
}

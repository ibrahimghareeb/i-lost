package com.example.lostfound.model;

import android.graphics.Picture;

import com.google.gson.annotations.SerializedName;

import java.io.File;
import java.io.Serializable;
import java.util.List;

public class ItemModel  {
    int idItem;
    String user_name;
    @SerializedName("email")
    String email;
    @SerializedName("mobile")
    String mobile;
    @SerializedName("nameItem")
    String nameItem;
    @SerializedName("description")
    String description;
    @SerializedName("latitude")
    String latitude;
    @SerializedName("longitude")
    String longitude;
    @SerializedName("city")
    String City;
    @SerializedName("Pictures")
    List<ItemPicture> Pictures;
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }



    public ItemModel(int idItem, String user_name, String mobile, String description, String nameItem, String latitude, String longitude, String city, List<ItemPicture> pictures) {
        this.idItem = idItem;
        this.user_name = user_name;
        this.mobile = mobile;
        this.description = description;
        this.nameItem = nameItem;
        this.latitude = latitude;
        this.longitude = longitude;
        City = city;
        Pictures = pictures;
    }

    public int getIdItem() {
        return idItem;
    }

    public void setIdItem(int idItem) {
        this.idItem = idItem;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNameItem() {
        return nameItem;
    }

    public void setNameItem(String nameItem) {
        this.nameItem = nameItem;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public List<ItemPicture> getPictures() {
        return Pictures;
    }

    public void setPictures(List<ItemPicture> pictures) {
        Pictures = pictures;
    }
}

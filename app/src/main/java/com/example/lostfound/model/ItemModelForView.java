package com.example.lostfound.model;

import android.graphics.Bitmap;

import java.io.Serializable;
@SuppressWarnings("serial")
public class ItemModelForView implements Serializable {
    public ItemModel itemModel;
    public Bitmap imageUrl;
    public ItemModelForView(ItemModel itemModel,Bitmap imageUrl) {
        this.itemModel = itemModel;
        this.imageUrl=imageUrl;
    }




}

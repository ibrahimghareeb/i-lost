package com.example.lostfound.model;

import java.io.File;

public class ItemPicture {
    String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public ItemPicture(String url) {
        this.url = url;
    }
}

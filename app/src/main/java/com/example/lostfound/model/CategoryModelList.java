package com.example.lostfound.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CategoryModelList {
    @SerializedName("data")
    List<CategoryModel> data;
    int Error;
    String alert;

    public CategoryModelList(List<CategoryModel> data, int error, String alert) {
        this.data = data;
        Error = error;
        this.alert = alert;
    }

    public List<CategoryModel> getData() {
        return data;
    }

    public void setData(List<CategoryModel> data) {
        this.data = data;
    }

    public int getError() {
        return Error;
    }

    public void setError(int error) {
        Error = error;
    }

    public String getAlert() {
        return alert;
    }

    public void setAlert(String alert) {
        this.alert = alert;
    }
}
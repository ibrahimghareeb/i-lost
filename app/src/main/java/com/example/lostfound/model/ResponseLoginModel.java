package com.example.lostfound.model;

public class ResponseLoginModel {
    int id;
    String email;
    double latitude;
    double longitude;
    String name;
    String alert;
    int Error;

    public ResponseLoginModel(int id, String email, double latitude, double longitude, String name, String alert, int error) {
        this.id = id;
        this.email = email;
        this.latitude = latitude;
        this.longitude = longitude;
        this.name = name;
        this.alert = alert;
        this.Error = error;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAlert(String alert) {
        this.alert = alert;
    }

    public void setError(int error) {
        this.Error = error;
    }

    public int getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public String getName() {
        return name;
    }

    public String getAlert() {
        return alert;
    }

    public int getError() {
        return Error;
    }
}

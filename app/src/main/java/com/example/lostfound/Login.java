package com.example.lostfound;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lostfound.model.ResponseLoginModel;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class Login extends AppCompatActivity {
    TextView signup;
    EditText email;
    EditText password;
    Button login;
    Retrofit retrofit;
    RetrofitInterface retrofitInterface;
    String BASE_URL="https://cars-leas.000webhostapp.com/lostthings/";
    SharedPreferences.Editor editor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        signup=(TextView) findViewById(R.id.signupTextView);
        email=(EditText) findViewById(R.id.email);
        password=(EditText) findViewById(R.id.editPassword);
        login =(Button)findViewById(R.id.login);
        retrofit =new Retrofit.Builder().
                baseUrl(BASE_URL).
                addConverterFactory(GsonConverterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();
        retrofitInterface = retrofit.create(RetrofitInterface.class);
        editor=getSharedPreferences("myuser",Context.MODE_PRIVATE).edit();
    }
    public void onSignupTextViewClicked(View v){
        Intent in=new Intent(Login.this,Signup.class);
        startActivity(in);
    }
    public boolean checkInputField(){
        if (email.getText().toString().equals("") || !email.getText().toString().contains("@")){
            Toast.makeText(Login.this,"Please enter valid email ",Toast.LENGTH_SHORT).show();
            return false;
        }
        if (password.getText().toString().equals("") || password.getText().toString().length()<6){
            Toast.makeText(Login.this,"Please Use 6 characters or more for your password ",Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
    public void loginNow(View v)  throws IOException {
        if(!checkInputField()){
            return;
        }

        Call<ResponseLoginModel> call=retrofitInterface.executeLogin(email.getText().toString(),password.getText().toString());
        call.enqueue(new Callback<ResponseLoginModel>() {
            @Override
            public void onResponse(Call<ResponseLoginModel> call, Response<ResponseLoginModel> response) {
                if(response.code()==200){
                    ResponseLoginModel responseBody=response.body();
                    if(responseBody.getError()==0){
                        editor.putBoolean("Login",true).commit();
                        editor.putString("email",email.getText().toString()).commit();
                        editor.putString("password",password.getText().toString()).commit();
                        editor.putInt("userId",responseBody.getId()).commit();
                        editor.putString("latitude",String.valueOf(responseBody.getLatitude())).commit();
                        editor.putString("longitude",String.valueOf(responseBody.getLongitude())).commit();
                        editor.putString("fullname",responseBody.getName()).commit();
                        Intent in=new Intent(Login.this,MainActivity.class);
                        startActivity(in);
                        finish();
                    }else{
                        Toast.makeText(Login.this,responseBody.getAlert(),Toast.LENGTH_LONG).show();
                    }
                }
            }
            @Override
            public void onFailure(Call<ResponseLoginModel> call, Throwable t) {
                Toast.makeText(Login.this,t.toString(),Toast.LENGTH_LONG).show();
            }
        });

    }

}